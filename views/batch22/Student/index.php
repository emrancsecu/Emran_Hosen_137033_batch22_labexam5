<?php
session_start();
include_once('../../../vendor/autoload.php');
use App\batch22\Student\Student;


$student= new Student();
$allstudent=$student->index();

?>
<!DOCTYPE html>
<html>
<head>

</head>
<body>

<h1>All Student List</h1>

<h2><a href="create.php">Create Student info</a></h2>

<table class="table">
            <thead>
            <tr>
                <th>#</th>
                <th>ID</th>
                <th>Firstname</th>
                <th>MiddleName</th>
                <th>LastName</th>

                <th>Action</th>

            </tr>
            </thead>
            <tbody>
            <tr>
                <?php
                $sl=0;
                foreach($allstudent as $student){
                $sl++; ?>
                <td><?php echo $sl?></td>
                <td><?php echo $student-> id?></td>
                <td><?php echo $student->firstname?></td>
                <td><?php echo $student->middlename?></td>
                <td><?php echo $student->lastname?></td>
                <td><a href="view.php?id=<?php echo $student-> id ?>" >View</a>
                    <a href="edit.php?id=<?php echo $student-> id ?>"  >Edit</a>
                    <a href="delete.php?id=<?php echo $student->id?>">Delete</a>

                </td>

            </tr>
            <?php }?>


            </tbody>
        </table>
    </div>
</div>


</body>
</html>
